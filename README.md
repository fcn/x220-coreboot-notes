# Coreboot, Tianocore and Thinkpad X220 Notes

This document and repo is not a standalone guide, just some extra information I had to figure out myself or gather together from different sources. I put it together to make it easier for others. I'm not sure you can directly flash any of the rom images found here, so if you are feeling adventurous, give it a try and let me know! (I don't take any responsibility if you brick your device!)

|Filename|Payload|
|--------|-------|
|coreboot-20200628-1429.rom	|Tianocore, libgfxinit, VGA Bios, custom splash (ibm.bmp), use CMOS, SMMM Data Store|
|coreboot-20200628-1503.rom	|Tianocore, libgfxinit, no VGA Bios, custom splash (ibm.bmp), use CMOS, SMMM Data Store|
|coreboot-20200628-2101.rom	|GRUB as payload |
|x220-original-bios.img		|Original BIOS Image (Whitelist removed version)|
|pci8086,0126.rom		|Extracted VGA Bios from https://github.com/thetarkus/x220-coreboot-guide/raw/master/vga-8086-0126.bin|


## Selection of configuration options

### Use CMOS for nvram

Under `General setup`, enable `Use CMOS for configuration values`. Otherwise you can't change options in the nvram. There is a tool called `nvramtool` in `utils` directory.

`~/coreboot/util/nvramtool (master) $ sudo ./nvramtool -a` should show you all available options. This is not available unless you compile coreboot `Use CMOS for configuration values` enabled. 

To swap FN with CTRL:
`~/coreboot/util/nvramtool (master) $ sudo ./nvramtool -w fn_ctrl_swap=Enable`

### Use SMM for Tianocore
Under `Generic Drivers`, `Support for flash based, SMM mediated data store` needs to be enabled. Otherwise you can't propagate boot options i.e. you can't add a new boot option or change boot order. As far as I understand these two are only two propagations you can do currently. 

Under `Payload`, `Tiaonocore payload` should be `CorebootPayload` as this is the custom branch from MrChromeBox with some improvements.

## How to set a custom bootsplash image for Tianocore?

Using GIMP, export the image as a `Windows BMP image` and disable `Do not write color space information` from Compatibility Options. Also select a 24-bit color range because apparently Tianocore tries to show a 24-bit image and if the image is saved as 16-bit or 32-bit, it just converts it to 24-bit on the fly. Just be safe and save it as 24-bits.

<img src="Screenshot_2020-06-28-14-39-06.png" width="420">
<img src="Screenshot_2020-06-28-14-40-05.png" width="420">

You should place the image in the coreboot root folder. The default filename is bootsplash.bmp, so you can just rename your bmp file to bootsplash.bmp or you can type the filename in `nconfig` (under `Payload`, `Use a custom bootsplash image`) and make coreboot find the image. If it can't find the image the compilation continues with a warning which is easy to miss for the eye, so if you don't see your custom bootsplash image after flashing your image just make sure to check the compilation logs. If coreboot can find the image but doesn't show it, your image maybe wrong in some way, sorry it didn't work out for you.

If the image filesize is too big, tianocore finishes compiling but the resulting UEFI payload becomes to big to fit in the designated space in the rom image so coreboot throws an error and doesn't finish compiling the image. My 556 Kb image file works fine but it's a colorful image and the filesize can go too high too quick since the filetype is BMP which doesn't offer much of an image compression I guess.

## Problems

* I tried setting up a coreboot compile environment in my Arch Linux installation but I wouldn't compile, so intead of spending time troubleshooting I set up a VM with Debian and compiled coreboot in an isolated environment. I guess its possible to get a docker image too but I don't know a lot about docker, so a VM seemed simpler.
* Tianocore with libgfxinit enabled: Can't boot Windows 7 though I actually haven't tried with Windows 10 yet.

## Resources and guides you'll find useful

* https://tylercipriani.com/blog/2016/11/13/coreboot-on-the-thinkpad-x220-with-a-raspberry-pi/
* https://github.com/michaelmob/x220-coreboot-guide
* https://szclsya.me/posts/coreboot/x220/
* https://karlcordes.com/coreboot-x220/
* https://notabug.org/Velsoth/x220-coreboot
* https://www.harmonicflow.org/blog/Flashing_coreboot_on_a_Lenovo_Thinkpad_X220_with_a_ch341a_USB_programmer_Tutorial.php
* http://www.zerocat.org/projects/coreboot-machines/doc/generated-documentation/html/md_doc_build-coreboot-x220.html

* IBM Bootsplash image: https://twitter.com/ibm/status/474635591758647296
